package kategoristatusgizi;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class konversi {

    private ArrayList<datamodel> dataset;

    private File file;

    public konversi(File file) {

        this.file = file;
        mulai_konversi();
    }

    private void mulai_konversi() {

        //inisialisasi arraylist dengan tipe data datamodel class yang dibuat
        dataset = new ArrayList<datamodel>();

        //inisialisasi class reader dari library apache common csv
        Reader reader = null;

        try {
            //melakukan parsing csv menjadi arraylist
            reader = Files.newBufferedReader(Paths.get(file.getAbsolutePath()));
            CSVParser parse = new CSVParser(reader, CSVFormat.DEFAULT);

            //melakukan pengulangan sebanyak banyaknya baris data yang ada pada file csv
            for (CSVRecord record : parse) {

                //mengakses dan menyimpan data sementara yang berada pada tiap kolom dalam baris data pada csv
                String name = record.get(0);
                String usia = record.get(1);
                String berat = record.get(2);
                String tinggi = record.get(3);

                //membuat class datamodel dan menambahkan data yang diambil dari csv pada variabel sementara
                datamodel data = new datamodel(name, Double.valueOf(usia), Double.valueOf(berat), Double.valueOf(tinggi));

                //menambahkan class data model ke arraylist
                dataset.add(data);
            }

        } catch (IOException ex) {

            ex.getStackTrace();

        } finally {

            try {

                reader.close();

            } catch (IOException ex) {

                ex.getStackTrace();
            }
        }
    }

    public ArrayList<datamodel> hasil_konversi() {

        return dataset;
    }
}
