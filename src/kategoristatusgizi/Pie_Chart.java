package kategoristatusgizi;

import java.awt.Font;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class Pie_Chart {

    private int cluster;
    private int sangatburuk, buruk, kurang, normal, lebih, obesitas;
    private DefaultPieDataset dataset;

    public Pie_Chart (int cluster, int sangatburuk, int buruk, int kurang, int normal, int lebih, int obesitas) {

        this.cluster = cluster;
        this.sangatburuk = sangatburuk;
        this.buruk = buruk;
        this.kurang = kurang;
        this.normal = normal;
        this.lebih = lebih;
        this.obesitas = obesitas;
        
        createDataset();
    }

    private PieDataset createDataset() {

        if (cluster == 2) {

            dataset = new DefaultPieDataset();
            dataset.setValue("Kurang", kurang);
            dataset.setValue("Normal", normal);
        
        } else if (cluster == 3) {
            
            dataset = new DefaultPieDataset();
            dataset.setValue("Kurang", kurang);
            dataset.setValue("Normal", normal);
            dataset.setValue("Lebih", lebih);
        
        } else if (cluster == 4) {
            
            dataset = new DefaultPieDataset();
            dataset.setValue("Buruk", buruk);
            dataset.setValue("Kurang", kurang);
            dataset.setValue("Normal", normal);
            dataset.setValue("Lebih", lebih);
            
        } else if (cluster == 5) {
            
            dataset = new DefaultPieDataset();
            dataset.setValue("Buruk", buruk);
            dataset.setValue("Kurang", kurang);
            dataset.setValue("Normal", normal);
            dataset.setValue("Lebih", lebih);
            dataset.setValue("Obesitas", obesitas);
            
        } else if (cluster == 6) {
            
            dataset = new DefaultPieDataset();
            dataset.setValue("Sangat Kurang", sangatburuk);
            dataset.setValue("Buruk", buruk);
            dataset.setValue("Kurang", kurang);
            dataset.setValue("Normal", normal);
            dataset.setValue("Lebih", lebih);
            dataset.setValue("Obesitas", obesitas);
            
        }
        
        return dataset;
    }

    public JPanel createChart() {

        JFreeChart chart = ChartFactory.createPieChart(
                "Kategori Status Gizi",
                dataset,
                true,
                true,
                false
        );

        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        plot.setNoDataMessage("No data available");
        plot.setCircular(false);
        plot.setLabelGap(0.02);
        
        return new ChartPanel(chart);
    }
}
