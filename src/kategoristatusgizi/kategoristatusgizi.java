package kategoristatusgizi;

import java.io.File;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class kategoristatusgizi extends javax.swing.JFrame {

    public kategoristatusgizi() {
        sangatburuk = buruk = kurang = normal = lebih = obesitas = 0;
        initComponents();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label_data_mentah = new javax.swing.JLabel();
        label_data_normalisasi = new javax.swing.JLabel();
        buttonGroup = new javax.swing.ButtonGroup();
        bukaPanel = new javax.swing.JPanel();
        bukaButton = new javax.swing.JButton();
        lokasiField = new javax.swing.JTextField();
        bukaLabel = new javax.swing.JLabel();
        tabContainer = new javax.swing.JTabbedPane();
        mentahScroller = new javax.swing.JScrollPane();
        mentah_Tabel = new javax.swing.JTable();
        normalScroller = new javax.swing.JScrollPane();
        normal_Tabel = new javax.swing.JTable();
        linkageScroller = new javax.swing.JScrollPane();
        linkage_Tabel = new javax.swing.JTable();
        mulaiButton = new javax.swing.JButton();
        sep1 = new javax.swing.JSeparator();
        jumlahPanel = new javax.swing.JPanel();
        jumlahLabel = new javax.swing.JLabel();
        radio3 = new javax.swing.JRadioButton();
        radio4 = new javax.swing.JRadioButton();
        radio5 = new javax.swing.JRadioButton();
        radio2 = new javax.swing.JRadioButton();
        radio6 = new javax.swing.JRadioButton();
        sep2 = new javax.swing.JSeparator();
        sep3 = new javax.swing.JSeparator();
        sep4 = new javax.swing.JSeparator();
        resetButton = new javax.swing.JButton();

        label_data_mentah.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        label_data_mentah.setText("Tabel Data Mentah");

        label_data_normalisasi.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        label_data_normalisasi.setText("Tabel Data Normalisasi");

        buttonGroup.add(radio3);
        buttonGroup.add(radio4);
        buttonGroup.add(radio5);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Kategori Status Gizi");
        setMinimumSize(new java.awt.Dimension(800, 600));
        setName("mainFrame"); // NOI18N
        setSize(new java.awt.Dimension(800, 600));

        bukaButton.setText("...");
        bukaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bukaButtonActionPerformed(evt);
            }
        });

        lokasiField.setEditable(false);

        bukaLabel.setText("Buka file :");

        javax.swing.GroupLayout bukaPanelLayout = new javax.swing.GroupLayout(bukaPanel);
        bukaPanel.setLayout(bukaPanelLayout);
        bukaPanelLayout.setHorizontalGroup(
            bukaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bukaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bukaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bukaPanelLayout.createSequentialGroup()
                        .addComponent(lokasiField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bukaButton))
                    .addGroup(bukaPanelLayout.createSequentialGroup()
                        .addComponent(bukaLabel)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        bukaPanelLayout.setVerticalGroup(
            bukaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bukaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bukaLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bukaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bukaButton)
                    .addComponent(lokasiField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mentah_Tabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nama", "Usia", "Berat", "Tinggi", "Kategori"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        mentahScroller.setViewportView(mentah_Tabel);
        if (mentah_Tabel.getColumnModel().getColumnCount() > 0) {
            mentah_Tabel.getColumnModel().getColumn(4).setHeaderValue("Kategori");
        }

        tabContainer.addTab("Data", mentahScroller);

        normal_Tabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nama", "Usia", "Berat", "Tinggi"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        normalScroller.setViewportView(normal_Tabel);
        if (normal_Tabel.getColumnModel().getColumnCount() > 0) {
            normal_Tabel.getColumnModel().getColumn(0).setResizable(false);
            normal_Tabel.getColumnModel().getColumn(1).setResizable(false);
            normal_Tabel.getColumnModel().getColumn(2).setResizable(false);
            normal_Tabel.getColumnModel().getColumn(3).setResizable(false);
        }

        tabContainer.addTab("Normalisasi", normalScroller);

        linkage_Tabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "", "", "", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        linkageScroller.setViewportView(linkage_Tabel);
        if (linkage_Tabel.getColumnModel().getColumnCount() > 0) {
            linkage_Tabel.getColumnModel().getColumn(0).setResizable(false);
            linkage_Tabel.getColumnModel().getColumn(1).setResizable(false);
            linkage_Tabel.getColumnModel().getColumn(2).setResizable(false);
            linkage_Tabel.getColumnModel().getColumn(3).setResizable(false);
            linkage_Tabel.getColumnModel().getColumn(4).setResizable(false);
        }

        tabContainer.addTab("Detail", linkageScroller);

        mulaiButton.setText("Mulai");
        mulaiButton.setEnabled(false);
        mulaiButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mulaiButtonActionPerformed(evt);
            }
        });

        sep1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jumlahLabel.setText("Jumlah (k) :");

        buttonGroup.add(radio3);
        radio3.setSelected(true);
        radio3.setText("3");
        radio3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio3ItemStateChanged(evt);
            }
        });

        buttonGroup.add(radio4);
        radio4.setText("4");
        radio4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio4ItemStateChanged(evt);
            }
        });

        buttonGroup.add(radio5);
        radio5.setText("5");
        radio5.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio5ItemStateChanged(evt);
            }
        });

        buttonGroup.add(radio2);
        radio2.setText("2");
        radio2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio2ItemStateChanged(evt);
            }
        });

        buttonGroup.add(radio6);
        radio6.setText("6");
        radio6.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radio6ItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jumlahPanelLayout = new javax.swing.GroupLayout(jumlahPanel);
        jumlahPanel.setLayout(jumlahPanelLayout);
        jumlahPanelLayout.setHorizontalGroup(
            jumlahPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jumlahPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jumlahPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jumlahPanelLayout.createSequentialGroup()
                        .addComponent(radio2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(radio3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radio4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radio5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(radio6))
                    .addComponent(jumlahLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jumlahPanelLayout.setVerticalGroup(
            jumlahPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jumlahPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jumlahLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jumlahPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radio3)
                    .addComponent(radio4)
                    .addComponent(radio5)
                    .addComponent(radio2)
                    .addComponent(radio6))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        sep3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        sep4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        resetButton.setText("Reset");
        resetButton.setEnabled(false);
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tabContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
                    .addComponent(sep2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bukaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sep1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jumlahPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sep3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mulaiButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sep4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(resetButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(bukaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sep1)
                    .addComponent(mulaiButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sep3)
                    .addComponent(sep4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(resetButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jumlahPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sep2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 491, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    //action ketika tombol buka di tekan
    private void bukaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bukaButtonActionPerformed

        //inisiasi JFileChooser untuk jendela open file
        JFileChooser pilih = new javax.swing.JFileChooser();
        //inisiasi file filter untuk tipe file tertentu
        FileNameExtensionFilter filter = new FileNameExtensionFilter("csv files", "csv");
        //menambahkan file filter untuk hanya menampilkan file csv
        pilih.addChoosableFileFilter(filter);
        pilih.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);

        //membuka jendela open file
        int returnValue = pilih.showOpenDialog(null);

        //melakukan pengecekan ketika jendela open file tertutup apakah open file atau cancel
        if (returnValue == javax.swing.JFileChooser.APPROVE_OPTION) {

            //mendapatkan lokasi file yang dibuka beserta nama dan tipe file
            file = new java.io.File(pilih.getSelectedFile().getAbsolutePath());

            //melakukan pengecekan tipe data csv apakah benar csv atau tidak
            if (pilih.getSelectedFile().getAbsolutePath().contains("csv")) {

                lokasiField.setText(pilih.getSelectedFile().getAbsolutePath());

                mulaiButton.setEnabled(true);
                resetButton.setEnabled(true);

            } else {
                //menampilkan pesan error ketika file yang dibuka bukan bertipe csv
                javax.swing.JOptionPane.showMessageDialog(null, "File tipe tidak valid, program hanya memproses file csv");
            }

        }
    }//GEN-LAST:event_bukaButtonActionPerformed

    //action ketika tombol mulai di tekan
    private void mulaiButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mulaiButtonActionPerformed

        hasil_SSE = new ArrayList<Double>();

        konversi();

        normalisasi_data();

        for (int i = 2; i < 7; i++) {

            if (i == cluster) {

                singlelinkage(true, cluster);
                hasil_SSE.add(kmeans(true, cluster));

            } else {

                singlelinkage(false, i);
                hasil_SSE.add(kmeans(false, i));
            }

            var_reset();
        }

        centroid_ecludean = new ecludean_centroid(K_centroid);
        jarak_centroid = centroid_ecludean.average_res();

        update_data();

        update_tabel_data();

        update_tabel_normal();

        update_tabel_linkage();

        update_tabel_centroid();

        update_tabel_sse();

        gambar_chart();
        
        gambar_pie();

    }//GEN-LAST:event_mulaiButtonActionPerformed

    private void konversi() {

        //memanggil method konversi
        konversi ubah = new konversi(file);

        dataset = ubah.hasil_konversi();
    }

    private void normalisasi_data() {

        //memanggil method normalisasi
        normalisasi normal = new normalisasi(dataset);

        dataset_normal = normal.hasil_normalisasi();
    }

    private void singlelinkage(boolean state, int k) {

        euclead = new ecludean_distance(dataset_normal);

        jarak = euclead.result();

        linkage = new single_linkage(jarak, k);

        index = linkage.index();

        centro = new centroid(dataset_normal, index);

        centroid = centro.hasil_centroid();

        if (k == cluster) {

            K_linkage = new ArrayList<ArrayList<Double>>(linkage.result());
            //K_centroid = new ArrayList<ArrayList<Double>>(centro.hasil_centroid());
        }
    }

    private Double kmeans(boolean state, int k) {

        Double sse = 0.0;

        //tambah_tab("Centroid", centro.hasil_centroid_text());
        means = new kmeans(centroid, dataset_normal, index, k);

        hasil_kmeans = means.hasil_kmeans();

        hasil_index = means.hasil_index();

        if (k == cluster) {

            K_index = new ArrayList<ArrayList<Integer>>(means.hasil_index());
            K_centroid = new ArrayList<ArrayList<Double>>(means.hasil_centroid());

        }

        SSE hsse = new SSE(hasil_index, hasil_kmeans);

        sse = hsse.hasil_sse();

        return sse;
    }

    private void update_data() {

        for (int i = 0; i < K_index.size(); i++) {

            for (int j = 0; j < K_index.get(i).size(); j++) {

                if (cluster == 2) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_2[i]);
                    
                    if (i == 0) {
                        
                        kurang = kurang + 1;
                    
                    } else {
                        
                        normal = normal + 1;
                    }
                }

                if (cluster == 3) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_3[i]);
                    
                    if (i == 0) {
                        
                        kurang = kurang + 1;
                    
                    } else if (i == 1) {
                        
                        normal = normal + 1;
                    
                    } else {
                        
                        lebih = lebih + 1;
                    }
                }

                if (cluster == 4) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_4[i]);
                    
                    if (i == 0) {
                        
                        buruk = buruk + 1;
                    
                    } else if (i == 1) {
                        
                        kurang = kurang + 1;
                    
                    } else if (i == 2) {
                        
                        normal = normal + 1;
                    
                    } else {
                        
                        lebih = lebih + 1;
                    } 
                }

                if (cluster == 5) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_5[i]);
                    
                    if (i == 0) {
                        
                        buruk = buruk + 1;
                    
                    } else if (i == 1) {
                        
                        kurang = kurang + 1;
                    
                    } else if (i == 2) {
                        
                        normal = normal + 1;
                    
                    } else if (i == 3) {
                        
                        lebih = lebih + 1;
                    
                    } else {
                        
                        obesitas = obesitas + 1;
                    }
                }

                if (cluster == 6) {

                    dataset.get(K_index.get(i).get(j)).update_kategori(kategori_status_6[i]);
                    
                    if (i == 0) {
                        
                        sangatburuk = sangatburuk + 1;
                    
                    } else if (i == 1) {
                        
                        buruk = buruk + 1;
                    
                    } else if (i == 2) {
                        
                        kurang = kurang + 1;
                    
                    } else if (i == 3) {
                        
                        normal = normal + 1;
                    
                    } else if (i == 4) {
                        
                        lebih = lebih + 1;
                    
                    } else {
                        
                        obesitas = obesitas + 1;
                    }
                }

            }
        }
    }

    public void var_reset() {

        hasil_index = null;
        hasil_kmeans = null;

        jarak.clear();
        index.clear();
        centroid.clear();
    }

    public void reset_all() {

        means = null;
        centro = null;
        linkage = null;
        euclead = null;

        index = null;
        hasil_index = null;
        K_index = null;
        jarak = null;
        centroid = null;
        hasil_kmeans = null;
        dataset = null;
        dataset_normal = null;
        hasil_SSE = null;
        //cluster = 3;
        file = null;
        centroid_ecludean = null;
        jarak_centroid = 0.0;
        
        sangatburuk = buruk = kurang = normal = lebih = obesitas = 0;

        if (tabContainer.getTabCount() > 3) {

            DefaultTableModel model_mentah = new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "Nama", "Usia", "Berat", "Tinggi", "Kategori"
                    }
            );

            mentah_Tabel.setModel(model_mentah);

            DefaultTableModel model_normal = new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "Nama", "Usia", "Berat", "Tinggi"
                    }
            );

            normal_Tabel.setModel(model_normal);

            DefaultTableModel model_linkage = new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "", "", "", "", ""
                    }
            );

            linkage_Tabel.setModel(model_linkage);

            int tab = tabContainer.getTabCount();

            tabContainer.remove(tab - 1);
            
            tab = tabContainer.getTabCount();

            tabContainer.remove(tab - 1);
        }

        lokasiField.setText("");
        mulaiButton.setEnabled(false);
        resetButton.setEnabled(false);
    }

    private void gambar_chart() {

        XY_Chart chart = new XY_Chart(hasil_SSE);

        javax.swing.JScrollPane pane = new javax.swing.JScrollPane();

        pane.setViewportView(chart.hasil_chart());

        tabContainer.addTab("Chart", pane);
    }
    
    private void gambar_pie() {
        
        Pie_Chart pie = new Pie_Chart(cluster, sangatburuk, buruk, kurang, normal, lebih, obesitas);
        
        javax.swing.JScrollPane pane = new javax.swing.JScrollPane();
        
        pane.setViewportView(pie.createChart());
    
        tabContainer.addTab("Pie Chart", pane);
    }

    private void update_tabel_data() {

        DefaultTableModel model = (DefaultTableModel) mentah_Tabel.getModel();

        for (int i = 0; i < dataset.size(); i++) {

            model.addRow(new Object[]{dataset.get(i).nama(), dataset.get(i).usia(), dataset.get(i).berat(), dataset.get(i).tinggi(), dataset.get(i).kategori()});
        }

        mentah_Tabel.setModel(model);
    }

    private void update_tabel_normal() {

        DefaultTableModel model = (DefaultTableModel) normal_Tabel.getModel();

        for (int i = 0; i < dataset_normal.size(); i++) {

            model.addRow(new Object[]{dataset_normal.get(i).nama(), dataset_normal.get(i).usia(), dataset_normal.get(i).berat(), dataset_normal.get(i).tinggi()});
        }

        normal_Tabel.setModel(model);
    }

    private void update_tabel_linkage() {

        DefaultTableModel model = (DefaultTableModel) linkage_Tabel.getModel();

        model.addRow(new Object[]{});

        model.addRow(new Object[]{"Single Linkage : "});

        model.addRow(new Object[]{});

        for (int i = 0; i < K_linkage.size(); i++) {

            model.addRow(K_linkage.get(i).toArray());
        }

        model.addRow(new Object[]{});
    }

    private void update_tabel_centroid() {

        DefaultTableModel model = (DefaultTableModel) linkage_Tabel.getModel();

        model.addRow(new Object[]{"CENTROID : "});
        model.addRow(new Object[]{});

        ArrayList<ArrayList<String>> temp = new ArrayList<ArrayList<String>>();

        for (int i = 0; i < K_centroid.size(); i++) {

            int val = i + 1;

            ArrayList<String> child = new ArrayList<String>();

            child.add("C " + val);

            for (int j = 0; j < K_centroid.get(i).size(); j++) {

                child.add(K_centroid.get(i).get(j).toString());
            }

            temp.add(child);
        }

        for (int i = 0; i < temp.size(); i++) {

            model.addRow(temp.get(i).toArray());
        }
    }

    private void update_tabel_sse() {

        DefaultTableModel model = (DefaultTableModel) linkage_Tabel.getModel();

        model.addRow(new Object[]{});

        model.addRow(new Object[]{"Nilai SSE : "});

        model.addRow(new Object[]{});

        ArrayList<ArrayList<String>> temp = new ArrayList<ArrayList<String>>();

        for (int i = 0; i < hasil_SSE.size(); i++) {

            int val = i + 2;

            ArrayList<String> child = new ArrayList<String>();

            child.add("K sama dengan " + val);

            child.add(hasil_SSE.get(i).toString());

            temp.add(child);
        }

        for (int i = 0; i < temp.size(); i++) {

            model.addRow(temp.get(i).toArray());
        }

        model.addRow(new Object[]{});

        model.addRow(new Object[]{"Jarak Antar Centroid : ", jarak_centroid});

        model.addRow(new Object[]{});
    }

    private void radio3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio3ItemStateChanged
        // TODO add your handling code here:
        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio3.getText());
        }
    }//GEN-LAST:event_radio3ItemStateChanged

    private void radio4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio4ItemStateChanged
        // TODO add your handling code here:

        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio4.getText());
        }
    }//GEN-LAST:event_radio4ItemStateChanged

    private void radio5ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio5ItemStateChanged
        // TODO add your handling code here:

        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio5.getText());
        }
    }//GEN-LAST:event_radio5ItemStateChanged

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        // TODO add your handling code here:

        reset_all();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void radio2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio2ItemStateChanged
        // TODO add your handling code here:
        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio2.getText());
        }
    }//GEN-LAST:event_radio2ItemStateChanged

    private void radio6ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radio6ItemStateChanged
        // TODO add your handling code here:
        int state = evt.getStateChange();

        if (state == java.awt.event.ItemEvent.SELECTED) {

            cluster = Integer.parseInt(radio6.getText());
        }
    }//GEN-LAST:event_radio6ItemStateChanged

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(kategoristatusgizi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(kategoristatusgizi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(kategoristatusgizi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(kategoristatusgizi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new kategoristatusgizi().setVisible(true);
            }
        });
    }

    private kmeans means;
    private centroid centro;
    private single_linkage linkage;
    private ecludean_distance euclead;

    private ecludean_centroid centroid_ecludean;
    private Double jarak_centroid;

    private ArrayList<ArrayList<Integer>> index, hasil_index, K_index;
    private ArrayList<ArrayList<Double>> jarak, centroid, hasil_kmeans, K_linkage, K_centroid;
    private ArrayList<datamodel> dataset, dataset_normal;
    private ArrayList<Double> hasil_SSE;
    
    private int sangatburuk, buruk, kurang, normal, lebih, obesitas;
    private String[] kategori_status_2 = {"Kurang", "Normal"};
    private String[] kategori_status_3 = {"Kurang", "Normal", "Lebih"};
    private String[] kategori_status_4 = {"Buruk", "Kurang", "Normal", "Lebih"};
    private String[] kategori_status_5 = {"Buruk", "Kurang", "Normal", "Lebih", "Obesitas"};
    private String[] kategori_status_6 = {"Sangat Buruk", "Buruk", "Kurang", "Normal", "Lebih", "Obesitas"};
    private int cluster = 3;

    private File file;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bukaButton;
    private javax.swing.JLabel bukaLabel;
    private javax.swing.JPanel bukaPanel;
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JLabel jumlahLabel;
    private javax.swing.JPanel jumlahPanel;
    private javax.swing.JLabel label_data_mentah;
    private javax.swing.JLabel label_data_normalisasi;
    private javax.swing.JScrollPane linkageScroller;
    private javax.swing.JTable linkage_Tabel;
    private javax.swing.JTextField lokasiField;
    private javax.swing.JScrollPane mentahScroller;
    private javax.swing.JTable mentah_Tabel;
    private javax.swing.JButton mulaiButton;
    private javax.swing.JScrollPane normalScroller;
    private javax.swing.JTable normal_Tabel;
    private javax.swing.JRadioButton radio2;
    private javax.swing.JRadioButton radio3;
    private javax.swing.JRadioButton radio4;
    private javax.swing.JRadioButton radio5;
    private javax.swing.JRadioButton radio6;
    private javax.swing.JButton resetButton;
    private javax.swing.JSeparator sep1;
    private javax.swing.JSeparator sep2;
    private javax.swing.JSeparator sep3;
    private javax.swing.JSeparator sep4;
    private javax.swing.JTabbedPane tabContainer;
    // End of variables declaration//GEN-END:variables

}
