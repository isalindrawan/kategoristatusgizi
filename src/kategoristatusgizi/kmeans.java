package kategoristatusgizi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class kmeans {

    private ArrayList<datamodel> dataset;
    private ArrayList<ArrayList<Double>> centroid, kmeans;
    private ArrayList<ArrayList<Integer>> index, index_sebelum;
    private int k;

    public kmeans(ArrayList<ArrayList<Double>> centroid, ArrayList<datamodel> dataset, ArrayList<ArrayList<Integer>> index, int k) {

        this.centroid = centroid;
        this.dataset = dataset;
        this.index = index;
        this.k = k;

        while (true) {

            hitung_kmeans();

            new_index_centroid();

            centroid centro = new centroid(dataset, index);

            centroid = centro.hasil_centroid();

            if (cek_index_centroid()) {

                break;
            }
        }
    }

    private void hitung_kmeans() {

        kmeans = new ArrayList<ArrayList<Double>>();

        for (int i = 0; i < dataset.size(); i++) {

            ArrayList<Double> child = new ArrayList<Double>();

            for (int j = 0; j < centroid.size(); j++) {

                Double usia = Math.pow(dataset.get(i).usia() - centroid.get(j).get(0), 2);
                Double berat = Math.pow(dataset.get(i).berat() - centroid.get(j).get(1), 2);
                Double tinggi = Math.pow(dataset.get(i).tinggi() - centroid.get(j).get(2), 2);

                Double temp = BigDecimal.valueOf(Math.sqrt(usia + berat + tinggi))
                        .setScale(5, RoundingMode.HALF_UP)
                        .doubleValue();

                child.add(temp);
            }

            kmeans.add(child);
        }
    }

    private void new_index_centroid() {

        index_sebelum = new ArrayList<ArrayList<Integer>>(index);

        index.clear();

        //index = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < k; i++) {

            ArrayList<Integer> child = new ArrayList<Integer>();

            for (int j = 0; j < kmeans.size(); j++) {

                Double min = kmeans.get(j).get(0);

                int temp = 0;

                for (int l = 0; l < kmeans.get(j).size(); l++) {

                    int usia_param_min = Double.compare(kmeans.get(j).get(l), min);

                    if (usia_param_min < 0) {

                        min = kmeans.get(j).get(l);
                        temp = l;
                    }
                }

                if (temp == i) {

                    child.add(j);
                }
            }

            index.add(child);
        }
    }

    private boolean cek_index_centroid() {

        boolean status = true;

        for (int i = 0; i < index.size(); i++) {

            if (index.get(i).size() != index_sebelum.get(i).size()) {

                status = false;

                break;
            }
        }

        return status;
    }

    public ArrayList<ArrayList<Integer>> hasil_index() {

        return index;
    }

    public ArrayList<ArrayList<Double>> hasil_kmeans() {

        return kmeans;
    }

    public ArrayList<ArrayList<Double>> hasil_centroid() {

        return centroid;
    }
}
