package kategoristatusgizi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class normalisasi {

    private Double usia_min, usia_max, berat_min, berat_max, tinggi_min, tinggi_max;
    private ArrayList<datamodel> dataset, result;

    public normalisasi(ArrayList<datamodel> dataset) {

        this.dataset = dataset;
        pre_process();
        normal_process();
    }

    private void pre_process() {

        //inisiasi variabel
        usia_min = dataset.get(0).usia();
        berat_min = dataset.get(0).berat();
        tinggi_min = dataset.get(0).tinggi();

        usia_max = dataset.get(0).usia();
        berat_max = dataset.get(0).berat();
        tinggi_max = dataset.get(0).tinggi();

        //melakukan pengulangan sebanyak dataset
        for (int i = 1; i < dataset.size(); i++) {

            //membandingkan data
            int usia_param_min = Double.compare(dataset.get(i).usia(), usia_min);
            int usia_param_max = Double.compare(dataset.get(i).usia(), usia_max);
            int berat_param_min = Double.compare(dataset.get(i).berat(), berat_min);
            int berat_param_max = Double.compare(dataset.get(i).berat(), berat_max);
            int tinggi_param_min = Double.compare(dataset.get(i).tinggi(), tinggi_min);
            int tinggi_param_max = Double.compare(dataset.get(i).tinggi(), tinggi_max);

            //apabila usia lebih kecil
            if (usia_param_min < 0) {

                usia_min = dataset.get(i).usia();
            }

            //apabila usia lebih besar
            if (usia_param_max > 0) {

                usia_max = dataset.get(i).usia();
            }

            if (berat_param_min < 0) {

                berat_min = dataset.get(i).berat();
            }

            if (berat_param_max > 0) {

                berat_max = dataset.get(i).berat();
            }

            if (tinggi_param_min < 0) {

                tinggi_min = dataset.get(i).tinggi();
            }

            if (tinggi_param_max > 0) {

                tinggi_max = dataset.get(i).tinggi();
            }
        }
    }

    private void normal_process() {
        //inisiasi variabel penyimpan dataset yang ternormalisasi
        result = new ArrayList<datamodel>();

        //melakukan pengulangan sebanyak banyaknya data
        for (int i = 0; i < dataset.size(); i++) {

            //perhitungan normalisasi dan menjadikan hasil operasi menjadi 5 digit dibelakang koma
            Double usia_normal = BigDecimal.valueOf((dataset.get(i).usia() - usia_min) / (usia_max - usia_min))
                    .setScale(5, RoundingMode.HALF_UP)
                    .doubleValue();
            Double berat_normal = BigDecimal.valueOf((dataset.get(i).berat() - berat_min) / (berat_max - berat_min))
                    .setScale(5, RoundingMode.HALF_UP)
                    .doubleValue();
            Double tinggi_normal = BigDecimal.valueOf((dataset.get(i).tinggi() - tinggi_min) / (tinggi_max - tinggi_min))
                    .setScale(5, RoundingMode.HALF_UP)
                    .doubleValue();

            //menambahkan hasil operasi ke variabel dataset normal
            result.add(new datamodel(dataset.get(i).nama(), usia_normal, berat_normal, tinggi_normal));
        }
    }

    public ArrayList<datamodel> hasil_normalisasi() {

        return result;
    }
}
