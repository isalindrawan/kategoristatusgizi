package kategoristatusgizi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class centroid {

    private ArrayList<ArrayList<Double>> centroid;
    private ArrayList<datamodel> dataset;
    private ArrayList<ArrayList<Integer>> index;

    public centroid(ArrayList<datamodel> dataset, ArrayList<ArrayList<Integer>> index) {

        this.dataset = dataset;
        this.index = index;

        hitung_centroid();
    }

    private void hitung_centroid() {

        centroid = new ArrayList<ArrayList<Double>>();

        for (int i = 0; i < index.size(); i++) {

            double usia = 0;
            double berat = 0;
            double tinggi = 0;

            for (int j = 0; j < index.get(i).size(); j++) {

                usia = usia + dataset.get(index.get(i).get(j)).usia();
                berat = berat + dataset.get(index.get(i).get(j)).berat();
                tinggi = tinggi + dataset.get(index.get(i).get(j)).tinggi();
            }

            Double usia_final = BigDecimal.valueOf(usia / index.get(i).size())
                    .setScale(5, RoundingMode.HALF_UP)
                    .doubleValue();
            Double berat_final = BigDecimal.valueOf(berat / index.get(i).size())
                    .setScale(5, RoundingMode.HALF_UP)
                    .doubleValue();
            Double tinggi_final = BigDecimal.valueOf(tinggi / index.get(i).size())
                    .setScale(5, RoundingMode.HALF_UP)
                    .doubleValue();

            ArrayList<Double> temp = new ArrayList<Double>();

            temp.add(usia_final);
            temp.add(berat_final);
            temp.add(tinggi_final);

            centroid.add(temp);
        }
    }

    public ArrayList<ArrayList<Double>> hasil_centroid() {

        return centroid;
    }
}
