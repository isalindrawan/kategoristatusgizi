package kategoristatusgizi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class ecludean_centroid {

    private ArrayList<ArrayList<Double>> dataset;
    private ArrayList<Double> temp;
    private ArrayList<ArrayList<Double>> result;
    private Double final_res;
    private int counter;

    public ecludean_centroid(ArrayList<ArrayList<Double>> dataset) {

        counter = 0;
        final_res = 0.0;

        this.dataset = dataset;

        ecludean();

    }

    private void ecludean() {

        Double kol1, kol2, kol3, kol_upd1, kol_upd2, kol_upd3, hasil;

        result = new ArrayList<ArrayList<Double>>();

        //pengulangan sebanyak besarnya data
        for (int i = 0; i < dataset.size(); i++) {

            //menyimpan variabel usia, berat, tinggi yang akan di operasikan
            kol1 = dataset.get(i).get(0);
            kol2 = dataset.get(i).get(1);
            kol3 = dataset.get(i).get(2);

            //inisiasi variabel penyimpan data sementara
            temp = new ArrayList<Double>();

            //pengulangan sebanyak besarnya data
            for (int j = i; j < dataset.size(); j++) {

                //mengambil data yang akan dioperasikan sebagai variabel ke dua
                kol_upd1 = dataset.get(j).get(0);
                kol_upd2 = dataset.get(j).get(1);
                kol_upd3 = dataset.get(j).get(2);

                //operasi perhitungan
                hasil = BigDecimal.valueOf(Math.sqrt(Math.pow(kol_upd1 - kol1, 2) + Math.pow(kol_upd2 - kol2, 2) + Math.pow(kol_upd3 - kol3, 2))).setScale(5, RoundingMode.HALF_UP).doubleValue();

                if (hasil != 0.0) {

                    counter++;
                }

                final_res = BigDecimal.valueOf(final_res + hasil).setScale(5, RoundingMode.HALF_UP).doubleValue();
            }

        }
    }

    public Double result() {

        return final_res;
    }

    public Double average_res() {

        return BigDecimal.valueOf(final_res / counter).setScale(5, RoundingMode.HALF_UP).doubleValue();
    }
}
